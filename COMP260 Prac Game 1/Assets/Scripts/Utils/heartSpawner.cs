﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class heartSpawner : MonoBehaviour {


    public PlayerMove PlayerMove;
    public heart heartprefab;
    public Rect spawnRect;
    public int nheart = 1;
    public int cheart = 0;

    public int heart = 6;
    public float timer = 0.0f;
    // Use this for initialization
    void Start()
    {

        PlayerMove = FindObjectOfType<PlayerMove>();
    }

    // Update is called once per frame
    void Update()
    {

        timer += Time.deltaTime;
        if (cheart < nheart)
        {

            // if (timer > beePeriod)

            if (timer > 8 & PlayerMove.count > 3)

            {
                // instantiate a bee
                heart heart = Instantiate(heartprefab);

                heart.transform.parent = transform;
                // give the bee a name and number
                heart.gameObject.name = "heart " + cheart;
                cheart = cheart + 1;

                // move the bee to a random position within
                // the spawn rectangle
                float x = spawnRect.xMin +
                Random.value * spawnRect.width;
                float y = spawnRect.yMin +
                Random.value * spawnRect.height;
                heart.transform.position = new Vector2(x, y);
                timer = 0;
                // timer = timer - beePeriod;
            }


            
        }
    }

    public void DestroyH()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        cheart = cheart - 1;
        timer = 0;
    }
}
