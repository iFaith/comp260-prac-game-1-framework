﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triangle : MonoBehaviour {

    // Use this for initialization
    private Transform t;
    private Transform player;
    private Rigidbody2D rb2d;


    private void Awake()
    {
        t = this.transform;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        if (Vector2.Distance(t.position, player.position) < 3f)
        {
            float speed = 50;
            rb2d.AddForce(Random.onUnitSphere * speed);
        }
      //  if (player)
        //    print(player.name + " is " + Distance().ToString() + " units from " + t.name);

       // else
       //     print("Player not found!");
    }

    private float Distance()
    {
        return Vector2.Distance(t.position, player.position);
    }
}
