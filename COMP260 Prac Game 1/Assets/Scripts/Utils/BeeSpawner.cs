﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {

    public BeeMove beePrefab;
    public Rect spawnRect;
    public int nBees = 5;
    private int cBees = 0;
    
    public float beePeriod = 2.0f;
    private float timer = 0.0f;

    public float minBeePeriod = 1.0f;
    public float maxBeePeriod = 5.0f;

    void Start()
    {
        
    }
    
    void Update () {
        timer += Time.deltaTime;
        if (cBees < nBees )
        {

            // if (timer > beePeriod)
            if (timer > 0)


            {
                // instantiate a bee
                BeeMove bee = Instantiate(beePrefab);

                bee.transform.parent = transform;
                // give the bee a name and number
                bee.gameObject.name = "Bee " + cBees;
                cBees = cBees + 1;

                // move the bee to a random position within
                // the spawn rectangle
                float x = spawnRect.xMin +
                Random.value * spawnRect.width;
                float y = spawnRect.yMin +
                Random.value * spawnRect.height;
                bee.transform.position = new Vector2(x, y);


               // timer = timer - beePeriod;
                timer = timer - Random.Range(minBeePeriod, maxBeePeriod);

            }
        }


    }


    public void spawnMorebee()
    {
        nBees++;

    }


}
