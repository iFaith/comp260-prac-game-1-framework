﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMove : MonoBehaviour
{

    public float destroyRadius = 1.0f;
    private BeeSpawner beeSpawner;
    private heartSpawner heartSpawner;
    public trispawner trispawner;
    private coinspawner coinspawner;
    public int count;
    public int sizecount;
    public int difficulty;
    public Text countText;
    public float Xscale = 0f;
    public float yscale = 0f;
    public HUD hud;
    public AudioClip coinCollideClip;
    private AudioSource coin;


    private bool sounds = true;


    public AudioClip overCollideClip;
    private AudioSource over;

    public AudioClip background;
    private AudioSource bg;

    public AudioClip heartC;
    private AudioSource heart;

    public float timer = 0.0f;

    // Use this for initialization
    void Awake()
    {

        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();
        coinspawner = FindObjectOfType<coinspawner>();
        heartSpawner = FindObjectOfType<heartSpawner>();
        trispawner = FindObjectOfType<trispawner>();
        count = 0;
        setCountText();
        coin = GetComponent<AudioSource>();
 

        over = GetComponent<AudioSource>();
        bg = GetComponent<AudioSource>();
        bg.PlayOneShot(background);
        bg.volume = 0.05f;

        heart = GetComponent<AudioSource>();

    }
    public float maxSpeed = 5.0f; // in metres per second

    void Update()
    {

        timer += Time.deltaTime;
        // get the input values
        Vector2 direction;
        direction.x = Input.GetAxis("Horizontal");
        direction.y = Input.GetAxis("Vertical");
        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;
        // move the object
        transform.Translate(velocity * Time.deltaTime);

    }

    public static implicit operator Transform(PlayerMove v)
    {
        throw new NotImplementedException();
    }
    void Sounds()
    {
        if (sounds == false)
        {
            AudioSource.Destroy(bg);

        }
    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Coin"))
        {
            collision.gameObject.SetActive(false);
            count++;
            setCountText();
            coin.PlayOneShot(coinCollideClip);



            Xscale = Xscale + 0.05f;
            yscale = yscale + 0.05f;
            transform.localScale = new Vector2(Xscale - 0.5f, yscale - 0.5f);
            coinspawner.Destroycoin();

            if (count == 3)
            {
                trispawner.spawnTri();
                difficulty = count + 3;
            }
            if (difficulty == count)
            {
                trispawner.spawnTri();
                difficulty = count + 3;
                beeSpawner.spawnMorebee();
            }

        }


        if (collision.gameObject.CompareTag("heart"))
        {
            //  collision.gameObject.SetActive(false);
            heart.PlayOneShot(heartC);
            if (Xscale > 0.9f)
            {
                Xscale = Xscale - 0.2f;
                yscale = yscale - 0.2f;
                transform.localScale = new Vector2(Xscale - 0.5f, yscale - 0.5f);
            }
            heartSpawner.DestroyH();
        }


        if (collision.gameObject.CompareTag("enemy") && sounds.Equals(true))
        {
            over.PlayOneShot(overCollideClip);
            Debug.Log("Game Over!");
            hud.GameOver();
            this.enabled = false;
            Invoke("gameover", 1);

        }


    }

    void gameover()
    {
        sounds = false;
        this.Sounds();
        Time.timeScale = 0;
    }

    void setCountText()
    {
        countText.text = "Count : " + count.ToString();
    }
}
