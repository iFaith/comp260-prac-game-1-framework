﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coinspawner : MonoBehaviour {

    public coincode coinPrefab;
    public Rect spawnRect;
    public int ncoin = 1;
    private int cCoin = 0;
    
    private float timer = 0.0f;

    public float minBeePeriod = 1.0f;
    public float maxBeePeriod = 5.0f;

    void Start()
    {

    }

    void Update()
    {
        timer += Time.deltaTime;
        if (cCoin < ncoin)
        {

            // if (timer > beePeriod)
            //if (timer > 0)


            //{
                // instantiate a bee
                coincode coin = Instantiate(coinPrefab);

                coin.transform.parent = transform;
                // give the bee a name and number
                coin.gameObject.name = "coin " + cCoin;
                cCoin = cCoin + 1;

                // move the bee to a random position within
                // the spawn rectangle
                float x = spawnRect.xMin +
                Random.value * spawnRect.width;
                float y = spawnRect.yMin +
                Random.value * spawnRect.height;
                coin.transform.position = new Vector2(x, y);


                // timer = timer - beePeriod;
               // timer = timer - Random.Range(minBeePeriod, maxBeePeriod);

           // }
        }


    }


    public void Destroycoin()
    {

        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
                cCoin = cCoin - 1;
        }
    }



