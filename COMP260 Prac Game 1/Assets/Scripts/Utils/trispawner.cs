﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trispawner : MonoBehaviour {

    public triangle triPrefab;
    public Rect spawnRect;
    
    private int cTri = 0;
    public int nTri = 2;
    private PlayerMove PlayerMove;
    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    public void Update () {

        if (cTri < nTri)
        {
            triangle Triangle = Instantiate(triPrefab);

            Triangle.transform.parent = transform;
            // give the bee a name and number
            Triangle.gameObject.name = "triangle " + cTri;
            cTri = cTri + 1;

            // move the bee to a random position within
            // the spawn rectangle
            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            Triangle.transform.position = new Vector2(x, y);
        }
        

    }

    public void spawnTri()
    {
        nTri ++;
    }
    }


